import { ErrorHandler, Injectable, Component } from '@angular/core';
import { HttpErrorResponse } from '@angular/common/http';
@Injectable()
export class HttpErrorsHandler implements ErrorHandler {
    constructor() {
    }
    handleError(error: Error | HttpErrorResponse) {
        if (error instanceof HttpErrorResponse) {
            // Server or connection error happened;
            console.warn('Server or connection error happened');
            if (!navigator.onLine) {
                // Handle offline error
                console.error('Server is offline', error.message);
                console.warn('Written by KhanhJS');
            } else {
                // Handle Http Error (error.status === 403, 404...)
                console.error('Server error occured (error.status === 403, 404...)', error.message);
                console.warn('Written by KhanhJS');
            }
        } else {
            // console.error('Client error occured :', error.message);
            // console.warn('Written by KhanhJS');
            // Handle Client Error (Angular Error, ReferenceError...)
        }
    }
}
