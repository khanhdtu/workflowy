import { Pipe, PipeTransform } from '@angular/core';
import { Tasks } from '../models/task.model';

@Pipe({
  name: 'sort'
})
export class SortPipe implements PipeTransform {

  transform(datas: Tasks[], args?: any): any {
    if (datas) {
      // console.log(datas)
      return datas.sort((a, b) => {
        // tslint:disable-next-line:radix
        return parseInt(a.cDate) - parseInt(b.cDate);
      });
    }
    // return datas;
  }

}
