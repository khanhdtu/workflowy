import { Injectable } from '@angular/core';
import { API_CONTS } from './../../constants/api.contants';
import { HttpClient } from '@angular/common/http';
import { Response } from './../../models/response.model';
@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private http: HttpClient) { }

  get() {
    return this.http.get<Response>(API_CONTS.GET_USERS);
  }
}
