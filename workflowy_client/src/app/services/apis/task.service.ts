import { Injectable } from '@angular/core';
import { API_CONTS } from '../../constants/api.contants';
import { HttpClient } from '@angular/common/http';
import { Response } from './../../models/response.model';
@Injectable({
  providedIn: 'root'
})
export class TaskService {

  constructor(
    private http: HttpClient,
  ) { }

  get() {
      return this.http.get<Response>(API_CONTS.GET_TASKS);
  }

  getByAdmin() {
    return this.http.get<Response>(API_CONTS.GET_TASKS_BY_ADMIN);
  }

  insert(body: any) {
    return this.http.post<Response>(API_CONTS.INSERT_TASK, body);
  }

  update(data) {
    return this.http.post<Response>(API_CONTS.UPDATE_TASK, data);
  }
}
