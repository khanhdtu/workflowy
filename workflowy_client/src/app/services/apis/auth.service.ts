import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { API_CONTS } from './../../constants/api.contants';
import { Response } from './../../models/response.model';
@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(private http: HttpClient) {}

  register(username: string, password: string) {
    return this.http.post<Response>(API_CONTS.REGISTER, {
      username: username,
      password: password
    });
  }

  login(username: string, password: string) {
    return this.http.post<Response>(API_CONTS.LOGIN, {
      username: username,
      password: password
    });
  }
}
