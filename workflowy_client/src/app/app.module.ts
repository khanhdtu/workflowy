import { BrowserModule } from '@angular/platform-browser';
import { NgModule, ErrorHandler } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { TreeviewModule } from 'ngx-treeview';
import { TreeModule } from 'angular-tree-component';
import { HttpClientModule } from '@angular/common/http';
import { AppRouterModule } from './app.routing';
import { CommonModule } from '@angular/common';
import { ModalModule } from 'ngx-bootstrap';
import { ContextMenuModule } from 'ngx-contextmenu';
import { FocusModule } from 'angular2-focus';
import { ToastrModule, ToastrService } from 'ngx-toastr';
import { AngularFireModule } from 'angularfire2';
import { AngularFireDatabaseModule } from 'angularfire2/database';

import { AppComponent } from './app.component';
import { MainComponent } from './containers/main/main.component';
import { NavbarComponent } from './containers/navbar/navbar.component';
import { SidebarComponent } from './containers/sidebar/sidebar.component';
import { FooterComponent } from './containers/footer/footer.component';
import { RegisterComponent } from './components/registration/register.component';
import { TasksComponent } from './components/tasks/tasks.component';
import { AssignComponent } from './components/assignment/assign.component';

import { TaskService } from './services/apis/task.service';
import { AuthService } from './services/apis/auth.service';
import { BsModalService } from 'ngx-bootstrap';
import { UserService } from './services/apis/user.service';
import { HttpErrorsHandler } from './helpers/errorHandle.helper';
import { LoginComponent } from './components/login/login.component';
import { SortPipe } from './pipes/sort.pipe';

@NgModule({
  declarations: [
    AppComponent,
    TasksComponent,
    MainComponent,
    NavbarComponent,
    SidebarComponent,
    FooterComponent,
    RegisterComponent,
    AssignComponent,
    LoginComponent,
    SortPipe,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    TreeModule,
    TreeviewModule.forRoot(),
    HttpClientModule,
    AppRouterModule,
    CommonModule,
    ModalModule.forRoot(),
    ContextMenuModule.forRoot(),
    FocusModule.forRoot()
  ],
  providers: [
    TaskService,
    AuthService,
    UserService,
    BsModalService,
    // ERROR HANDLE GLOBAL
    {
      provide: ErrorHandler,
      useClass: HttpErrorsHandler
    },
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
