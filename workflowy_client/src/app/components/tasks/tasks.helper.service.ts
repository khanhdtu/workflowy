import { TREE_ACTIONS, IActionMapping, ITreeOptions, KEYS, TreeComponent, } from 'angular-tree-component';
import { Injectable } from '@angular/core';
import { Nodes } from '../../models/nodes.model';
import * as _ from 'lodash';
@Injectable()
export class TaskHelperService {
    public nodeOptions: ITreeOptions = {
        displayField: 'nm',
        isExpandedField: 'expanded',  getChildren: (node) => {
            return node.id;
          },
        idField: 'id',
        hasChildrenField: 'nodes',
        childrenField: 'ch',
        actionMapping: {
            mouse: {
                dblClick: (tree, node, $event) => {
                    const z = _.keys(tree.expandedNodeIds);
                    // console.log(z)
                    setTimeout(() => {
                        for ( let i = 0 ; i < z.length; i++) {
                            delete z[i];
                        }
                        console.log(z);
                        // console.log(_.keys(tree.expandedNodeIds))
                    }, 3000);
                    // if (node.hasChildren) { TREE_ACTIONS.TOGGLE_EXPANDED(tree, node, $event); }
                }
            },
            keys: {
                [KEYS.ENTER]: ( tree, node, $event ) => {
                    // console.log('sadasdasd')
                    // node.expandAll();
                },
                [KEYS.UP]: ( tree, node, $event ) => {
                    // node.expandAll();
                },
            }
        },
        allowDrag: (node) => {
            return true;
        },
        allowDrop: (node) => {
            return true;
        },
        useVirtualScroll: true,
        animateExpand: true,
        scrollOnActivate: true,
        animateSpeed: 30,
        animateAcceleration: 1.2,
        scrollContainer: document.body.parentElement
    };
}
