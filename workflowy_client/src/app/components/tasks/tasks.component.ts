import { Component, OnInit, Input, ViewChild, ElementRef } from '@angular/core';
import { TaskService } from '../../services/apis/task.service';
import { Nodes } from '../../models/nodes.model';
import { TaskHelperService } from './tasks.helper.service';
import { ITreeOptions, TreeComponent, TreeModel, TreeNode } from 'angular-tree-component';
import * as _ from 'lodash';
import { Tasks } from '../../models/task.model';
import { ContextMenuComponent } from 'ngx-contextmenu';
import { KEYBOARDS_CONST } from './../../constants/keyboard.constant';
import * as $ from 'jquery';
import { ActivatedRoute } from '@angular/router';
@Component({
  selector: 'app-tasks',
  templateUrl: './tasks.component.html',
  styleUrls: ['./tasks.component.scss'],
  providers: [TaskHelperService]
})
export class TasksComponent implements OnInit {
  // @ViewChild(ContextMenuComponent) public basicMenu: ContextMenuComponent;
  // TreeView template

  @ViewChild(TreeComponent)
  public tree: TreeComponent;
  public nodes: Nodes;
  public options: ITreeOptions;
  public title: string;
  public over: boolean;
  @ViewChild(ContextMenuComponent) public basicMenu: ContextMenuComponent;
  constructor(
    private taskService: TaskService,
    private taskHelperService: TaskHelperService,
    // private route: ActivatedRoute
  ) {
    this.options = this.taskHelperService.nodeOptions;
    this.title = '';
    this.over = false;
  }

  onEdit(event: any, node: any, key: string) {
    node.data[key] = event.target.value;
    // update
    const edited = _.differenceWith(
      this.nodes.current,
      this.nodes.old,
      _.isEqual
    )[0];
    // console.log(node.data);
    this.taskService.update({ task: edited })
    .toPromise()
    .then((e) => {
      if (node.data.id === 'undefined') {
        node.data = e.datas;
      }
      this.nodes.old = JSON.parse(JSON.stringify(this.nodes.current));
    });
  }

  onKey(event, node: any) {
    if (event.keyCode === KEYBOARDS_CONST.ENTER && !event.shiftKey && !event.ctrlKey) {
      if (node.realParent) {
        const newNode: Tasks = {
          id: (new Date().getTime()).toString(),
          nm: '',
          no: '',
          ch: [],
          complete: false,
          users: [],
          cDate: (new Date().getTime()).toString(),
          flags: { over: false, no_checked: false }
        };
        const index = node.parent.data.ch.findIndex(e => {
          return e.id === node.data.id;
        });
        node.parent.data.ch.splice(index + 1, 0, newNode);
        // update
        const edited = _.differenceWith(
          this.nodes.current,
          this.nodes.old,
          _.isEqual
        )[0];
        this.taskService.update({ task: edited })
          .toPromise()
          .then(() => {
            this.nodes.old = JSON.parse(JSON.stringify(this.nodes.current));
            this.tree.treeModel.update();
            setTimeout(() => {
              $(`.${newNode.id}`).focus();
            }, 100);
          });
      } else {
        const newNode: Tasks = {
          id: 'undefined',
          nm: '',
          no: '',
          ch: [],
          complete: false,
          users: [],
          cDate: (new Date().getTime()).toString(),
          flags: { over: false, no_checked: false }
        };
        const index = this.nodes.current.findIndex(e => {
          return e.id === node.data.id;
        });
        if (node.data.ch.length === 0) {
          this.nodes.current.splice(index + 1, 0, newNode);
          // update
        } else {
          node.data.ch.splice(0, 0, newNode);
        }
        // this.taskService.insert({
        //   title: '',
        //   cDate: node.data.cDate
        // })
        //   .toPromise()
        //   .then(e => {
        //     this.nodes.current.splice(index + 1, 0, newNode);
        //     this.nodes.current[index + 1] = e.datas;
        //     this.tree.treeModel.update();
        //     setTimeout(() => {
        //       $(`.${e.datas.id}`).focus();
        //     }, 100);
        //     this.nodes.old = JSON.parse(JSON.stringify(this.nodes.current));
        //     this.tree.treeModel.update();
        //     setTimeout(() => {
        //       $(`.${newNode.id}`).focus();
        //     }, 100);
        //   });
        this.tree.treeModel.update();
        setTimeout(()=>{
          $(`.${newNode.id}`).focus();
        },100)
      }

    } else if (event.keyCode === KEYBOARDS_CONST.ENTER && event.shiftKey) {
      node.data.flags.no_checked = true;
      setTimeout(() => {
        $(`#${node.data.id}`).focus();
      }, 100);
    } else if ( event.keyCode === KEYBOARDS_CONST.ENTER && event.ctrlKey) {
      node.data.complete = true;
      // update
      const edited = _.differenceWith(
        this.nodes.current,
        this.nodes.old,
        _.isEqual
      )[0];
      this.taskService.update({ task: edited })
        .toPromise()
        .then(() => {
          this.nodes.old = JSON.parse(JSON.stringify(this.nodes.current));
        });
    } else if (event.keyCode === KEYBOARDS_CONST.TAB) {
    } else if (event.keyCode === KEYBOARDS_CONST.BACK && event.ctrlKey && event.shiftKey) {
      const focused = _.keys(this.nodes.state.expandedNodeIds);
      // console.log(focused);
      // if (focused.length === 0) {
      //     const parentIndex = this.nodes.current.findIndex(e => {
      //     return e.id === this.nodes.state.focusedNodeId;
      //   });
      //   this.nodes.current.splice(parentIndex, 1);
      // }

      // this.tree.treeModel.update();
      setTimeout(() => {
        this.nodes.state.expandedNodeIds = [];
        console.log(this.nodes.state.expandedNodeIds);
      }, 3000);
    } else if (event.keyCode === KEYBOARDS_CONST.BACK && !event.ctrlKey) {
      if (!node.data.no) {
        node.data.flags.no_checked = false;
        // update
        const edited = _.differenceWith(
          this.nodes.current,
          this.nodes.old,
          _.isEqual
        )[0];
        this.taskService.update({ task: edited })
          .toPromise()
          .then(() => {
            this.nodes.old = JSON.parse(JSON.stringify(this.nodes.current));
          });
      }
    }
  }

  onGetMenuContent(e) {
    // console.log(e);
  }

  onEvent(e) {
    console.log(e);
  }

  onOptions(node) {
    node.data.flags.over = true;
    this.nodes.old = JSON.parse(JSON.stringify(this.nodes.current));
  }

  onLeave(node) {
    node.data.flags.over = false;
    this.nodes.old = JSON.parse(JSON.stringify(this.nodes.current));
  }

  onMove() {
    const nodesUpdated = _.differenceWith(
      this.nodes.current,
      this.nodes.old,
      _.isEqual
    )[0];
    const nodeDelete = _.differenceBy(
      this.nodes.old,
      this.nodes.current,
      'id'
    )[0];
    // console.log(nodesUpdated, nodeDelete)
    this.taskService.update({ task: nodesUpdated, nodeDelete: nodeDelete, type: 'move' })
      .toPromise()
      .then(() => {
        // Update old nodes again
        this.nodes.old = JSON.parse(JSON.stringify(this.nodes.current));
      })
      .catch(e => {
        // to do sth ...
      });
  }

  onInsert() {
    this.taskService.insert({ title: this.title })
      .toPromise()
      .then(newTask => {
        this.title = '';
        this.nodes.current.push(newTask.datas);
        this.nodes.old = JSON.parse(JSON.stringify(this.nodes.current));
        // console.log(this.nodes);
        this.tree.treeModel.update();
      });
  }

  onComplete(node) {
    node.data.complete = true;
    // console.log(this.nodes);
    const edited = _.differenceWith(
      this.nodes.current,
      this.nodes.old,
      _.isEqual
    )[0];
    // console.log('eeeee:', edited);
    this.taskService.update(edited)
      .toPromise()
      .then(() => {
        this.nodes.old = JSON.parse(JSON.stringify(this.nodes.current));
        // console.log(this.nodes);
      });
  }

  onStateChange(tree?: any) {
    // this.nodes.state = tree;
    // console.log(this.nodes.state.expandedNodeIds);
  }

  ngOnInit() {
    this.taskService.get()
    .toPromise()
    .then(e => {
        // console.log(e)
        this.nodes = {
          current: JSON.parse(JSON.stringify(e.datas)),
          old: JSON.parse(JSON.stringify(e.datas))
        };
      });
  }
}
