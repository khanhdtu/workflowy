import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../services/apis/auth.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss'],
})
export class RegisterComponent implements OnInit {
  public username: string;
  public password: string;
  public message: string;
  constructor(private authService: AuthService) {
    this.username = '';
    this.password = '';
    this.message = '';
  }

  onCreate() {
    this.authService.register(this.username, this.password)
    .toPromise()
    .then(success => {
      this.message = success.message;
      setTimeout(() => {
        this.message = '';
      }, 3000);
    })
    .catch(error => {

    });
  }

  ngOnInit() {
  }

}
