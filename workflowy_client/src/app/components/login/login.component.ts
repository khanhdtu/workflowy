import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { AuthService } from '../../services/apis/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  @Output() logged = new EventEmitter<any>();
  public username: string;
  public password: string;
  public message: string;
  constructor(
    private authService: AuthService,
    private router: Router
  ) {
    this.username = '';
    this.password = '';
    this.message = '';
  }

  onLogin() {
    this.authService.login(this.username, this.password)
    .toPromise()
    .then(e => {
      if (!e.datas) {
        this.message = e.message;
        setTimeout(() => {
          this.message = '';
        }, 3000);
      } else {
        localStorage.setItem('User', JSON.stringify(e.datas));
        this.router.navigate(['Home']);
      }
    });
  }

  ngOnInit() {
  }

}
