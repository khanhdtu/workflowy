import { Component, OnInit, ViewChild, TemplateRef  } from '@angular/core';
import { Nodes } from '../../models/nodes.model';
import { TaskService } from '../../services/apis/task.service';
import { TreeComponent, ITreeOptions } from 'angular-tree-component';
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
import { Tasks } from '../../models/task.model';
import { UserService } from '../../services/apis/user.service';
import { User } from '../../models/user.model';
import * as _ from 'lodash';
@Component({
  selector: 'app-assign',
  templateUrl: './assign.component.html',
  styleUrls: ['./assign.component.scss']
})
export class AssignComponent implements OnInit {
  @ViewChild(TreeComponent)
  public tree: TreeComponent;
  public nodes: Nodes;
  public options: ITreeOptions;
  public modalRef: BsModalRef;
  public users: Array<User>;
  public taskIdCurrent: string;
  constructor(
    private taskService: TaskService,
    private userService: UserService,
    private modalService: BsModalService
  ) {
    this.options = {
      displayField: 'nm',
    };
    this.users = [];
  }

  onOpenModal(template: TemplateRef<any>, task: Tasks ) {
    this.taskIdCurrent = task._id;
    this.modalRef = this.modalService.show(template);
    this.users.forEach(user => {
      if (task.users.includes(user.username)) {
        user.flag = true;
      } else {
        user.flag = false;
      }
    });
  }

  onUpdate() {
    // tslint:disable-next-line:prefer-const
    let temp = [];
    this.users.forEach(user => {
      if (user.flag) {
        temp.push(user.username);
      }
    });
    this.taskService.update({
      _id: this.taskIdCurrent,
      users: temp
    })
    .toPromise()
    .then(e => {
      alert(e.message);
    });
  }

  ngOnInit() {
    this.taskService.getByAdmin()
    .toPromise()
    .then(e => {
      this.nodes = {
        current : e.datas
      };
    });

    this.userService.get()
    .toPromise()
    .then(users => {
      this.users = users.datas;
    });
  }
}
