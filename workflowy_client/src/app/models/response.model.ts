export interface Response {
    status?: string;
    message?: string;
    datas?: any;
    timestamp?: number;
    error?: any;
}
