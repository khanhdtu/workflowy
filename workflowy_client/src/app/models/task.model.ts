export interface Tasks {
    _id?: string;
    id?: string;
    nm?: string;
    no?: string;
    ch?: any;
    complete?: boolean;
    users?: string[];
    cDate?: string;
    flags?: {
        no_checked?: boolean,
        over?: boolean
    };
}
