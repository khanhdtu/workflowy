export interface User {
    _id?: string;
    username?: string;
    password?: string;
    flag?: boolean;
    userRight?: string;
}
