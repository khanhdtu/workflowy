// tslint:disable-next-line:class-name
export class API_CONTS {

    public static ROOT = 'http://localhost:3000';
    // public static ROOT = 'https://angular.io/';

    public static TASK = `${API_CONTS.ROOT}/tasks`;
    public static AUTH = `${API_CONTS.ROOT}/auth`;
    public static USER = `${API_CONTS.ROOT}/users`;

    public static GET_TASKS = `${API_CONTS.TASK}`;
    public static GET_TASKS_BY_ADMIN = `${API_CONTS.TASK}/admin`;
    public static INSERT_TASK = `${API_CONTS.TASK}/insert`;
    public static UPDATE_TASK = `${API_CONTS.TASK}/update`;

    public static GET_USERS = `${API_CONTS.USER}`;

    public static REGISTER = `${API_CONTS.AUTH}/register`;
    public static LOGIN = `${API_CONTS.AUTH}/login`;
}
