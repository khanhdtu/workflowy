// tslint:disable-next-line:class-name
export class KEYBOARDS_CONST {
    public static BACK = 8;
    public static TAB = 9;
    public static ENTER = 13;
    public static SHIFT = 16;
    public static CTRL = 17;
    public static ALT = 18;
    public static LEF = 37;
    public static UP = 38;
    public static RIGHT = 39;
    public static DOWN = 40;
}
