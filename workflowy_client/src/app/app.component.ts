import { Component , OnChanges, ViewChild } from '@angular/core';
import { User } from './models/user.model';
import { Router } from '@angular/router';
import { AuthService } from './services/apis/auth.service';

@Component({
  selector: 'app-root',
  template: `
  <app-navbar></app-navbar>
  <div style="width: 100%;height: 100%">
    <app-main></app-main>
  </div>
  `,
})

export class AppComponent {
  public user: User;
  public auth = false;
  constructor(private router: Router) {
    if (!localStorage.getItem('User')) {
      this.router.navigate(['Login']);
    } else {
      this.auth = true;
    }
  }
}
